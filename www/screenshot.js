var exec = require('cordova/exec');
function Screenshot() {
}

Screenshot.prototype.load = function (url, successCallback, errorCallback) {
    exec(successCallback, errorCallback, 'Screenshot', 'load', [url]);
};

Screenshot.prototype.execJS = function (jsString, successCallback, errorCallback) {
    exec(successCallback, errorCallback, 'Screenshot', 'execJS', [jsString]);
};

Screenshot.prototype.takeScreenshots = function (filePath, successCallback, errorCallback) {
    exec(successCallback, errorCallback, 'Screenshot', 'takeScreenshots', [filePath]);
};

Screenshot.prototype.unload = function (successCallback, errorCallback) {
    exec(successCallback, errorCallback, 'Screenshot', 'unload');
};

module.exports = new Screenshot();