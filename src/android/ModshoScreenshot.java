package com.cbi.screenshot;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.renderscript.ScriptIntrinsicResize;
import android.renderscript.Type;
import android.util.Log;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class ModshoScreenshot extends CordovaPlugin {
  private static final String LOG_TAG = "ModshoScreenshot";

  private WebView screenshotWebView;
  private CallbackContext loadContext;

  public ModshoScreenshot() {
  }

  @Override
  public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
    if (action.equals("load")) {
      String url = args.getString(0);
      cordova.getActivity().runOnUiThread(() ->
        this.load(url, callbackContext)
      );
      return true;
    }
    if (action.equals("execJS")) {
      String jsString = args.getString(0);
      cordova.getActivity().runOnUiThread(() ->
        this.execJS(jsString, callbackContext)
      );
      return true;
    }
    if (action.equals("takeScreenshots")) {
      JSONArray params = args.getJSONArray(0);
      cordova.getActivity().runOnUiThread(() ->
        this.takeScreenshots(params, callbackContext)
      );
      return true;
    }
    if (action.equals("unload")) {
      cordova.getActivity().runOnUiThread(() ->
        this.unload(callbackContext)
      );
      return true;
    }

    return false;
  }

  private void load(String url, CallbackContext callbackContext) {
    if (url != null && url.length() > 0) {
      init();
      loadContext = callbackContext;
      screenshotWebView.loadUrl(url);
    } else {
      callbackContext.error("load: invalid url");
    }
  }

  private void execJS(String jsString, CallbackContext callbackContext) {
    if (jsString != null && jsString.length() > 0) {
      screenshotWebView.evaluateJavascript(jsString, callbackContext::success);
    } else {
      callbackContext.error("execJS: invalid jsString");
    }
  }

  private void takeScreenshots(JSONArray params, CallbackContext callbackContext) {
    if (params == null || params.length() == 0) {
      callbackContext.error("takeScreenshots: invalid argument");
      return;
    }

    // Take screenshot of the webView
    Bitmap fullBitmap = Bitmap.createBitmap(screenshotWebView.getWidth(), screenshotWebView.getHeight(), Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(fullBitmap);
    screenshotWebView.draw(canvas);

    cordova.getThreadPool().execute(() -> {
      // Resize and save each format
      String error = null;
      for (int i = 0; i < params.length(); i++) {
        try {
          JSONObject screenshot = (JSONObject) params.get(i);
          String filePath = screenshot.getString("file").replace("file://", "");
          JSONArray size = screenshot.getJSONArray("size");
          int w = size.getInt(0);
          int h = size.getInt(1);
          Bitmap scaled = resizeBitmap(fullBitmap, w);
          saveBitmap(scaled, filePath);
          scaled.recycle();
        } catch (Exception e) {
          error = e.getMessage();
          break;
        }
      }
      fullBitmap.recycle();

      if (error != null) {
        callbackContext.error(error);
      } else {
        callbackContext.success();
      }
    });

    /*
    screenshotWebView.measure(View.MeasureSpec.makeMeasureSpec(
      View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED),
      View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
    screenshotWebView.layout(0, 0, screenshotWebView.getMeasuredWidth(),
      screenshotWebView.getMeasuredHeight());
    screenshotWebView.setDrawingCacheEnabled(true);
    screenshotWebView.buildDrawingCache();
    Bitmap bm = Bitmap.createBitmap(screenshotWebView.getMeasuredWidth(),
      screenshotWebView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

    Canvas bigcanvas = new Canvas(bm);
    Paint paint = new Paint();
    int iHeight = bm.getHeight();
    bigcanvas.drawBitmap(bm, 0, iHeight, paint);
    screenshotWebView.draw(bigcanvas);
    System.out.println("1111111111111111111111="
      + bigcanvas.getWidth());
    System.out.println("22222222222222222222222="
      + bigcanvas.getHeight());
    */
  }

  private void unload(CallbackContext callbackContext) {
    release();
    callbackContext.success("unload");
  }

  private void init() {
    if (screenshotWebView != null) {
      return;
    }

    final Activity activity = this.cordova.getActivity();

    // Create screenshot webView
    screenshotWebView = new WebView(activity);
    screenshotWebView.getSettings().setJavaScriptEnabled(true);
    RelativeLayout.LayoutParams webViewParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    screenshotWebView.setLayoutParams(webViewParams);

      /*
      screenshotWebView.setWebChromeClient(new WebChromeClient() {
        public void onProgressChanged(WebView view, int progress) {
        }
      });
      */

    screenshotWebView.setWebViewClient(new WebViewClient() {
      @Override
      public void onPageStarted(WebView view, String url, Bitmap favicon) {
        Log.d(LOG_TAG, "onPageStarted: " + url);
      }

      @Override
      public void onPageFinished(WebView view, String url) {
        Log.d(LOG_TAG, "onPageFinished: " + url);
        PluginResult res = new PluginResult(PluginResult.Status.OK);  // By default "OK" is sent as message !
        res.setKeepCallback(true);
        loadContext.sendPluginResult(res);
      }

      @Override
      public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        Log.e(LOG_TAG, "onReceivedError: " + description);
        PluginResult res = new PluginResult(PluginResult.Status.ERROR, description);
        res.setKeepCallback(false);
        loadContext.sendPluginResult(res);
      }

      @Override
      public boolean shouldOverrideUrlLoading(WebView view, String request) {
        Uri uri = Uri.parse(request);

        if (uri.getScheme().equals("event")) {
          Log.d(LOG_TAG, "Event: " + request);
          PluginResult res = new PluginResult(PluginResult.Status.OK, request);
          res.setKeepCallback(true);
          loadContext.sendPluginResult(res);
          return true;
        }

        return false;
      }
    });

    //  Add to parent view
    FrameLayout parentLayout = (FrameLayout) webView.getView().getParent();
    parentLayout.addView(screenshotWebView, 0);
  }

  private void release() {
    if (screenshotWebView == null) {
      return;
    }

    FrameLayout parentLayout = (FrameLayout) webView.getView().getParent();
    parentLayout.removeView(screenshotWebView);
    loadContext = null;
    screenshotWebView.setWebChromeClient(null);
    screenshotWebView.setWebViewClient(null);
    screenshotWebView.stopLoading();
    screenshotWebView = null;
  }

  /*
  private Bitmap resizeBitmap(Bitmap original, int w, int h) {
    return Bitmap.createScaledBitmap(original, w, h, true);
  }

  public Bitmap resizeBitmap(Bitmap bitmap, int newWidth, int newHeight) {
    Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);

    float ratioX = newWidth / (float) bitmap.getWidth();
    float ratioY = newHeight / (float) bitmap.getHeight();
    float middleX = newWidth / 2.0f;
    float middleY = newHeight / 2.0f;

    Matrix scaleMatrix = new Matrix();
    scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

    Canvas canvas = new Canvas(scaledBitmap);
    canvas.setMatrix(scaleMatrix);
    canvas.drawBitmap(bitmap, middleX - bitmap.getWidth() / 2, middleY - bitmap.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

    return scaledBitmap;
  }
  */

  // https://medium.com/@petrakeas/alias-free-resize-with-renderscript-5bf15a86ce3
  public Bitmap resizeBitmap(Bitmap src, int dstWidth) {
    RenderScript rs = RenderScript.create(cordova.getActivity());
    Bitmap.Config bitmapConfig = src.getConfig();
    int srcWidth = src.getWidth();
    int srcHeight = src.getHeight();
    float srcAspectRatio = (float) srcWidth / srcHeight;
    int dstHeight = (int) (dstWidth / srcAspectRatio);

    float resizeRatio = (float) srcWidth / dstWidth;

    // Calculate gaussian's radius
    float sigma = resizeRatio / (float) Math.PI;
    // https://android.googlesource.com/platform/frameworks/rs/+/master/cpu_ref/rsCpuIntrinsicBlur.cpp
    float radius = 2.5f * sigma - 1.5f;
    radius = Math.min(25, Math.max(0.0001f, radius));

    // Gaussian filter
    Allocation tmpIn = Allocation.createFromBitmap(rs, src);
    Allocation tmpFiltered = Allocation.createTyped(rs, tmpIn.getType());
    ScriptIntrinsicBlur blurIntrinsic = ScriptIntrinsicBlur.create(rs, tmpIn.getElement());

    blurIntrinsic.setRadius(1);
    blurIntrinsic.setInput(tmpIn);
    blurIntrinsic.forEach(tmpFiltered);

    tmpIn.destroy();
    blurIntrinsic.destroy();

    // Resize
    Bitmap dst = Bitmap.createBitmap(dstWidth, dstHeight, bitmapConfig);
    Type t = Type.createXY(rs, tmpFiltered.getElement(), dstWidth, dstHeight);
    Allocation tmpOut = Allocation.createTyped(rs, t);
    ScriptIntrinsicResize resizeIntrinsic = ScriptIntrinsicResize.create(rs);

    resizeIntrinsic.setInput(tmpFiltered);
    resizeIntrinsic.forEach_bicubic(tmpOut);
    tmpOut.copyTo(dst);

    tmpFiltered.destroy();
    tmpOut.destroy();
    resizeIntrinsic.destroy();

    return dst;
  }

  private void saveBitmap(Bitmap bm, String path) throws IOException {
    String dirPath = path.substring(0, path.lastIndexOf(File.separatorChar));
    File dir = new File(dirPath);
    if (!dir.isDirectory() && !dir.mkdirs()) {
      Log.e(LOG_TAG, "Error creating directories: " + dirPath);
      return;
    }

    OutputStream fOut;
    File file = new File(path);
    fOut = new FileOutputStream(file);

    bm.compress(Bitmap.CompressFormat.PNG, 100, fOut);  // Quality is ignored with PNG format (lossless)
    fOut.flush();
    fOut.close();
  }
}
