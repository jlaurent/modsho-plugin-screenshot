//
//  NSFileManager-Extension.m
//  Presenter
//
//  Created by Julien Laurent Cbi Multimédia on 03/06/13.
//  Copyright (c) 2013 Julien Laurent Cbi Multimédia. All rights reserved.
//

#import "NSFileManager+Extension.h"

@implementation NSFileManager (AutoOverWrite)

- (BOOL)copyItemAtPath:(NSString*)srcPath
                toPath:(NSString*)dstPath
                 error:(NSError*__autoreleasing *)error
             overwrite:(BOOL)over
{
    if (over && [self fileExistsAtPath:dstPath])
    {
        if (![self removeItemAtPath:dstPath error:error])
            return NO;
    }
    
    if (![self createDirectoryAtPath:[dstPath stringByDeletingLastPathComponent]
        withIntermediateDirectories:YES attributes:nil error:error])
    {
        return NO;
    }
    
    return [self copyItemAtPath:srcPath toPath:dstPath error:error];
}

- (BOOL)createIntermediatesDirectoriesForFileAtPath:(NSString *)path attributes:(NSDictionary *)attr
{
    NSString* folder = [path stringByDeletingLastPathComponent];
    if (![self fileExistsAtPath:folder])
    {
        NSError* error;
        if (![self createDirectoryAtPath:folder withIntermediateDirectories:YES attributes:attr error:&error])
            return NO;
    }
    return YES;
}

- (BOOL)createFileAtPathWithIntermediatesDirectories:(NSString*)path contents:(NSData*)data attributes:(NSDictionary*)attr
{
    BOOL created = [self createIntermediatesDirectoriesForFileAtPath:path attributes:attr];
    if (!created)
        return NO;

    return [self createFileAtPath:path contents:data attributes:attr];
}

- (void)deleteFileAtPath:(NSString* )path
{
    NSError* error;
    NSFileManager* fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:path])
    {
        if (![fm removeItemAtPath:path error:&error])
            NSLog(@"Failed to delete file %@.\n%@ %@", path, [error localizedDescription], [error localizedFailureReason]);
    }
}

@end
