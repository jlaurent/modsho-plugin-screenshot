//
//  NSFileManager-Extension.h
//  Presenter
//
//  Created by Julien Laurent Cbi Multimédia on 03/06/13.
//  Copyright (c) 2013 Julien Laurent Cbi Multimédia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager (AutoOverWrite)

- (BOOL)copyItemAtPath:(NSString*)srcPath toPath:(NSString*)dstPath error:(NSError*__autoreleasing *)error overwrite:(BOOL)over;
- (BOOL)createFileAtPathWithIntermediatesDirectories:(NSString*)path contents:(NSData*)data attributes:(NSDictionary*)attr;
- (BOOL)createIntermediatesDirectoriesForFileAtPath:(NSString*)path attributes:(NSDictionary*)attr;
- (void)deleteFileAtPath:(NSString*)path;

@end
