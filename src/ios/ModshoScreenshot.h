#import <UIKit/UIKit.h>
#import <Cordova/CDVPlugin.h>

@interface ModshoScreenshot : CDVPlugin <UIWebViewDelegate>
{}

- (void)load:(CDVInvokedUrlCommand*)command;
- (void)execJS:(CDVInvokedUrlCommand*)command;
- (void)takeScreenshots:(CDVInvokedUrlCommand*)command;
- (void)unload:(CDVInvokedUrlCommand*)command;

@end
