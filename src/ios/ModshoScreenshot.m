#import <Cordova/CDV.h>
#import "ModshoScreenshot.h"
#import "UIImage+Resize.h"
#import "NSFileManager+Extension.h"

static BOOL kDebugMode = NO;

@interface ModshoScreenshot()
{
    UIWebView* webView;
    CDVInvokedUrlCommand* loadCommand;
}
@end

@implementation ModshoScreenshot

/*
- (void)pluginInitialize
{
    [super pluginInitialize];
}
*/

- (void)dealloc
{
    [self releaseWebView];
}

// JS commands
- (void)load:(CDVInvokedUrlCommand*)command
{
    //[self clearWebView];
    
    loadCommand = command;
    NSString* url = [command.arguments objectAtIndex:0];
    if (kDebugMode) {
        NSLog(@"ModshoScreenshot load %@", url);
    }

    if (url != nil && [url length] > 0) {
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
        [self initWebView];
        [webView loadRequest:request];
    } else {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"URL must be provided"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)execJS:(CDVInvokedUrlCommand*)command
{
    NSString* cmd = [command.arguments objectAtIndex:0];
    if (kDebugMode) {
        NSLog(@"ModshoScreenshot execJS %@", cmd);
    }
    
    NSString* res = [webView stringByEvaluatingJavaScriptFromString:cmd];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:res];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)takeScreenshots:(CDVInvokedUrlCommand*)command
{
    NSArray* params = [command.arguments objectAtIndex:0];
    if (kDebugMode) {
        NSLog(@"ModshoScreenshot takeScreenshots %@", params);
    }
    
    // Take screenshot of the webview
    CGSize s = webView.frame.size;
    UIGraphicsBeginImageContextWithOptions(s, YES, 0.0);
        [webView.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage* source = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    [self.commandDelegate runInBackground:^{
        // Resize and save each asked format
        NSString* errorMsg;
        for (NSDictionary* screenshot in params) {
            @try {
                NSString* filePath = [screenshot valueForKey:@"file"];
                NSArray* size = [screenshot valueForKey:@"size"];
                NSNumber* w = [size objectAtIndex:0];
                NSNumber* h = [size objectAtIndex:1];
                UIImage* processed = [source resizedImage:CGSizeMake([w floatValue], [h floatValue]) interpolationQuality:kCGInterpolationHigh];
                filePath = [filePath stringByReplacingOccurrencesOfString:@"file://" withString:@""];
                errorMsg = [self writeImage:processed toFile:filePath];
                if (errorMsg) {
                    break;
                }
            } @catch (NSError* e) {
                errorMsg = [NSString stringWithFormat:@"%@ %@", [e description], [e localizedDescription]];
                break;
            }
        };

        CDVPluginResult* pluginResult;
        if (errorMsg) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:errorMsg];
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        }
        [pluginResult setKeepCallbackAsBool:NO];
        
        //[self performSelectorOnMainThread:@selector(clearWebView) withObject:Nil waitUntilDone:YES];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)unload:(CDVInvokedUrlCommand*)command
{
    [self releaseWebView];
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:command.callbackId];
}

// Privates
- (void)initWebView
{
    if (webView) {
        return;
    }
    
    webView = [[UIWebView alloc] init];
    [webView setDelegate:self];
    [webView setScalesPageToFit:NO];
    [[webView scrollView] setBounces:NO];
    
    UIView* parentView = self.viewController.view;
    [parentView insertSubview:webView atIndex:[[parentView subviews]count]-1];
    //[parentView addSubview:webView];
    [webView setFrame:parentView.frame];
}

-(void)releaseWebView
{
    if (!webView) {
        return;
    }
    
    [webView setDelegate:nil];
    [webView stopLoading];
    [webView removeFromSuperview];
    webView = Nil;
}

-(void)clearWebView
{
    [webView stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML = \"\";"];
    [webView stopLoading];
}

- (NSString*)webViewPath:(UIWebView *)wv
{
    NSURLRequest* rq = [wv request];
    NSURL* url = [rq URL];
    return [url absoluteString];
}

- (NSString*)writeImage:(UIImage*)image toFile:(NSString*)path
{
    NSData* data;
    NSError* err;
    NSString* ext = [[path pathExtension]lowercaseString];
    
    if ([ext isEqualToString:@"jpg"])
        data = UIImageJPEGRepresentation(image, 1.0);
    else if ([ext isEqualToString:@"png"])
        data = UIImagePNGRepresentation(image);
    else
        return [NSString stringWithFormat:@"Unknow image extension %@", ext];
    
    NSFileManager* fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:path])
        [fm deleteFileAtPath:path];
    [fm createIntermediatesDirectoriesForFileAtPath:path attributes:nil];
    
    BOOL res = [data writeToFile:path options:NSDataWritingAtomic error:&err];
    if (!res)
        return [NSString stringWithFormat:@"Failed to write encoded image to file %@ %@ %@", path, [err description], [err localizedDescription]];
                
    return Nil;
}

#pragma mark - Webview delegate
- (void)webViewDidStartLoad:(UIWebView *)wv
{
    if (kDebugMode) {
        NSLog(@"ModshoScreenshot webViewDidStartLoad %@", [self webViewPath:wv]);
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)wv
{
    if (kDebugMode) {
        NSLog(@"ModshoScreenshot webViewDidFinishLoad %@", [self webViewPath:wv]);
    }
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [pluginResult setKeepCallbackAsBool:YES];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:loadCommand.callbackId];
}

- (void)webView:(UIWebView *)wv didFailLoadWithError:(NSError *)error
{
    /*
    if ([error code] == -999) {
        NSLog(@"ModshoScreenshot webViewDidFailLoadWithError skipped -999 for %@", [self webViewPath:wv]);
        return;
    }
    */
    
    if (kDebugMode) {
        NSLog(@"ModshoScreenshot webViewDidFailLoadWithError %@ \n %@ \n %@", [self webViewPath:wv], [error description], [error localizedDescription]);
    }
    NSString* errMsg = [NSString stringWithFormat:@"%@ %@", [error description], [error localizedDescription]];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:errMsg];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:loadCommand.callbackId];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType
{
    NSString* requestString = [[request URL] absoluteString];
    //NSString* escapedString = [requestString stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLPathAllowedCharacterSet];
    if ([requestString hasPrefix:@"event:"])
    {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:requestString];
        if (kDebugMode) {
            NSLog(@"ModshoScreenshot event %@", requestString);
        }
        [pluginResult setKeepCallbackAsBool:YES];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:loadCommand.callbackId];

        return NO;
    }
    
    return YES;
}


@end